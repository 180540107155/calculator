package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView tvdisplay,number1,number2,number3,number4,number5,number6,number7,number8,number9,number0,delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initViewRefernce();
        clickButton();



        delete.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                String string = tvdisplay.getText().toString();
                if(!string.equals("")){
                    StringBuilder value = new StringBuilder(string);
                    tvdisplay.setText(value.deleteCharAt(value.length()-1));
                }
            }
        });
    }

    void initViewRefernce(){
        tvdisplay = findViewById(R.id.tvActDisplay);

        number1 = findViewById(R.id.tvActNumber1);
        number2 = findViewById(R.id.tvActNumber2);
        number3 = findViewById(R.id.tvActNumber3);
        number4 = findViewById(R.id.tvActNumber4);
        number5 = findViewById(R.id.tvActNumber5);
        number6 = findViewById(R.id.tvActNumber6);
        number7 = findViewById(R.id.tvActNumber7);
        number8 = findViewById(R.id.tvActNumber8);
        number9 = findViewById(R.id.tvActNumber9);
        number0 = findViewById(R.id.tvActNumber0);

        delete = findViewById(R.id.tvActDelete);
    }

    void clickButton(){
        number1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvdisplay.setText(tvdisplay.getText().toString() + "1");

            }
        });
        number2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvdisplay.setText(tvdisplay.getText().toString() + "2");

            }
        });
        number3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvdisplay.setText(tvdisplay.getText().toString() + "3");

            }
        });
        number4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvdisplay.setText(tvdisplay.getText().toString() + "4");

            }
        });
        number5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvdisplay.setText(tvdisplay.getText().toString() + "5");

            }
        });
        number6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvdisplay.setText(tvdisplay.getText().toString() + "6");

            }
        });
        number7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvdisplay.setText(tvdisplay.getText().toString() + "7");

            }
        });
        number8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvdisplay.setText(tvdisplay.getText().toString() + "8");

            }
        });
        number9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvdisplay.setText(tvdisplay.getText().toString() + "9");

            }
        });
        number0.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                tvdisplay.setText(tvdisplay.getText().toString() + "0");

            }
        });
    }


}